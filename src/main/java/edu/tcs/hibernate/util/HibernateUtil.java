package edu.tcs.hibernate.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static ServiceRegistry registry;
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				
				//Create configuration
				Configuration configuration=new Configuration().configure();
				
				
				// Create registry
			    registry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

				// Create SessionFactory
				sessionFactory = configuration.buildSessionFactory(registry);	

			} catch (Exception e) {
				e.printStackTrace();
				}

		}
		return sessionFactory;
	}

	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}
