package edu.tcs.hibernate.main;

import org.hibernate.Session;
import org.hibernate.Transaction;

import edu.tcs.hibernate.model.Student;
import edu.tcs.hibernate.util.HibernateUtil;

public class App {

	public static void main(String[] args) {

		Student student = new Student("Guillermo", "HTML");
		Transaction transaction =null;
		try(Session session = HibernateUtil.getSessionFactory().openSession())
		{
			
		//Start transaction
			transaction = session.beginTransaction();
		//Save student object
			session.save(student);
		//Commit transaction	
			transaction.commit();
		}catch(Exception e) {
			e.printStackTrace();
		}
	
	}
}
